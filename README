Lua-tools je sada funkcií a tried (table_range, vm, result, istack_stream, ...)
uľahčujúcich prácu s lua zásobníkom. Nasleduje pár ukážiek použitia knižnice.

Čítanie tabuľky pomocou triedy table \code{.cpp}

	// {1, 2, name='Lisbon'}

	table tbl(res);
	int i1 = tbl.at<int>(1);               // i1=1
	string name = tbl.at<string>("name");  // name="Lisbon"
	tbl.at<int>("name") = 101;
	int iname = tbl.at<int>("name");       // iname=101

\endcode. Tabuľku (a tiež aj table_range) je možné použiť spolu s
for-range \code

	lua::script_engine lvm(lmessage);
	lvm.load_script("test.lua");
	lua::result res = lvm.call_function("return_mixed_table");
	for (table_pair kv : lua::table(res))
		...

\endcode, alebo iterovať iba cez reťazcové klúče \code{.cpp}

	for (auto v : lua::table(res)|lua::only_string_keys)
		...

\endcode

Polia tabuľky pomocou členskej funkcie field \code{.cpp}

	bool module = tb.field("module", true);

\endcode

Čítanie s heterogennej tabuľky, iteračná metóda [Ukážka:

	// {1, 2, name='Lisbon'}

	for (lua::table_range r(res); r; ++r, ++idx)
	{
		std::pair<int, int> types = *r;
		if (types.first == LUA_TSTRING)
		{
			if (r.key() == "name")
				BOOST_CHECK_EQUAL(r.value<string>(), string("Lisbon"));
		}
		else
			BOOST_CHECK_EQUAL(idx+1, r.value<int>());
	}

--- koniec ukážky].

Volanie lua funkcie pomocou call_function() a spracovanie jej výsledku triedou
istack_stream [Example:

	#include "luatools.hpp"

	int main(int argc, char * argv[])
	{
		lua::script_engine lvm(lmessage);
		lvm.load_script("test.lua");

		lua::result r = lvm.call_function("person_data", "Jack Black");  // #1

		int salaty, age;
		lua::istack_stream(r) >> salary >> age;

		cout << "Jack Black:\n" 
			<< "  age   : " << age << "\n"
			<< "  salary: " << salary << "\n";

		r.flush();
	}

--- end example]. Riadok #1 zavolá lua funkciu person_data vracajúcu dve
celočíselné hodnoty (roky a príjem), pozri 'test.lua'.

Čítanie s homogennej tabuľky [Ukážka:

	lua::result r = lvm.call_function("age_table");
	map<string, int> ages;
	lua::istack_stream(r) >> ages;
	r.flush();

--- koniec ukážky].

Volanie funkcie s tabuľkou ako parametrom [Ukážka:

	lua::ostack_stream(lvm.state()) << lua::newtable 
		<< lua::tab("name", "Peter") << lua::tab("age", 20);

	lvm.call_function_raw("echo_table", 1);

--- koiec ukážky].

Volanie funkcie s užívateľsky definovanou štruktúrou, ktorá sa v lue
objavý ako tabuľka [Ukážka:

	struct person {
		string name;
		int age;
	};

	namespace lua {

	template <>
	inline void stack_push<person>(lua_State * L, person const & p) {
		ostack_stream(L) << newtable << tab("name", p.name) << tab("age", p.age);
	}

	}  // lua

	void foo() {
		person p = {"John Rambo", 38};
		lua::result r = lvm.call_function("custom_structure_test", p);	
	}

--- koniec ukážky].

